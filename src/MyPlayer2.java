import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer2 extends Player {

    Random rand;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer2() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();

        Move bestMove = search(gameBoard, 9, this.playerNumber, -1.0, 1.0);
        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber, double low, double high) {

        Move currentMove; // Hold current move and its value
        Move bestMove = new Move(0);// Hold best move found and its value
        bestMove.value = -1.0;
        Board board = new Board(gameBoard);
// Run through possible moves
        for (int i = 0; i <= 6; i++) {
            if(!board.isColumnOpen(i)) {
                continue;
            }
            //initial moves
            if(gameBoard.getBoard()[0][3] == 0){
                return new Move(3);
            }
            else if(gameBoard.getBoard()[1][3] == 0){
                return new Move(3);
            }
            else if(gameBoard.getBoard()[0][2] == 0){
                return new Move(2);
            }
            else if(gameBoard.getBoard()[0][4] == 0){
                return new Move(4);
            }
            // Place a tile in column i
            currentMove = new Move(i);
            board.move(playerNumber, i);

            int gameStatus = board.checkIfGameOver(i);
            if (gameStatus >= 0) {
                if (gameStatus == 0) {
                       // Tie game
                    currentMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                       // Win
                    currentMove.value = 1.0;
                } else {
                       // Loss
                    currentMove.value = -1.0;
                }
            }
            else if (maxDepth == 0) {
                currentMove.value = heuristic(board, playerNumber);
            }
            else { // New turn, Player change, reduce search depth
                currentMove = search(board, maxDepth - 1, (playerNumber == 1 ? 2 : 1), -high, -low);
                currentMove.value = -currentMove.value; // Good for opponent is bad for me
            }
            if (currentMove.value > bestMove.value) { // Found a new best move?
                bestMove = currentMove;
                low = Math.max(low, bestMove.value); // Update the low value, also
            }

        }//return bestMove;
        return bestMove;
    }

    public double heuristic(Board gameBoard, int playerNumber) {

        double score = 0.0;
        for (int r = 0; r < 6; r++) {
            for (int c = 0; c < 6; c++) {
                score += score(gameBoard, playerNumber, r, c);
            }
        }
        score = score/1000;

        return score;
        // This should return a number between -1.0 and 1.0.
        //
        // If the board favors playerNumber then the return value should be close to 1.0
        // If the board favors playerNumber's opponent then the return value should be close to 1.0
        // If the board favors neither player then the return value should be close to 0.0

    }
    public int score(Board gameBoard, int playerNumber, int row, int col) {
        int score;
        int threesTally = 0;
        int twosTally = 0;
        //int r, c;
        if(gameBoard.getBoard()[row][col] == playerNumber) {
            if(row < 6 && gameBoard.getBoard()[row + 1][col] == playerNumber){//check 1 above
                twosTally++;
                if(row > 0 && gameBoard.getBoard()[row - 1][col] == playerNumber){//checks if this is the middle of a three, is so, no need to count it as another set of three
                    twosTally--;
                }
                if(row < 5 && gameBoard.getBoard()[row + 2][col] == playerNumber){//check 2 above
                    threesTally++;
                    twosTally--;
                }
            }
            if(col < 6 && gameBoard.getBoard()[row][col + 1] == playerNumber){//check 1 right
                twosTally++;
                if(col > 0 && gameBoard.getBoard()[row][col - 1] == playerNumber){//checks if this is the middle of a three, is so, no need to count it as another set of three
                    twosTally--;
                }
                if(col < 5 && gameBoard.getBoard()[row][col + 2] == playerNumber){//check 2 right
                    threesTally++;
                    twosTally--;
                }
            }
            if(row < 6 && col < 6 && gameBoard.getBoard()[row + 1][col + 1] == playerNumber){//checks 1 up right(diagonal)
                twosTally++;
                if(row > 0 && col > 0 && gameBoard.getBoard()[row - 1][col - 1] == playerNumber){//checks if middle of a three, is so, no need to count it as another set of three
                    twosTally--;
                }
                if(row < 5 && col < 5 && gameBoard.getBoard()[row + 2][col + 2] == playerNumber){//checks 2 up right(diagonal)
                    threesTally++;
                    twosTally--;
                }
            }
            if(row < 6 && col > 0 && gameBoard.getBoard()[row + 1][col - 1] == playerNumber){//checks 1 up left(diagonal)
                twosTally++;
                if(row > 0 && col < 6 && gameBoard.getBoard()[row - 1][col + 1] == playerNumber){//checks if middle of a three
                    twosTally--;
                }
                if(row < 5 && col > 1 && gameBoard.getBoard()[row + 2][col - 2] == playerNumber){//checks 2 up left(diagonal)
                    threesTally++;
                    twosTally--;
                }
            }

        }
        score = (3 * threesTally) + (2 * twosTally);
        return score;
    }


    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }

        return bestMove;
    }



}
